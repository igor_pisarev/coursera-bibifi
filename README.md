# README #

This is for a capstone project of the Coursera Cybersecurity specialization (May 2015). It involves implementation of a secure system according to the specification while ensuring correctness & performance.

It is done with C# and Mono Framework (just for fun) although my primary language has been C++ for years.

### How do I get set up? ###

* Download & install *Mono*
* Run *make* from the *build* directory (or additionally install *MonoDevelop* and build from the IDE)

### Project specification ###
https://www.builditbreakit.org/static/doc/spring2015/spec/SPEC.html