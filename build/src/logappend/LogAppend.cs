﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using common;

namespace logappend
{
    class LogAppendRequest
    {
        public string Token;
        public string LogFilePath;
        public LogRecord Event;
    }

	class LogAppend
	{
        const string ARG_TIMESTAMP = "-T";
        const string ARG_TOKEN = "-K";
        const string ARG_EMPLOYEE = "-E";
        const string ARG_GUEST = "-G";
        const string ARG_ARRIVAL = "-A";
        const string ARG_LEAVING = "-L";
        const string ARG_ROOM = "-R";
        const string ARG_BATCH = "-B";


        static bool BatchMode;
        static string BatchFilePath;

        class BatchLog
        {
            public string Token;
            public StringBuilder Text;
        }
        static Dictionary<string, BatchLog> BatchLogs;
        static Dictionary<string, LogRecord> BatchLogsLastRecord;
        static Dictionary<string, Dictionary<PersonType, Dictionary<string, LogRecord>>> BatchLogsLastRecordByPerson;
        // meaning: Dictionary<LogFile, Dictionary<PersonType, Dictionary<PersonName, LastRecord>>>
        // sorry, i know this looks a bit ugly


		public static void Main(string[] args)
		{
            if (args.Length < 2)
                EnvironmentUtils.Invalid();

            try
            {
                LogAppendRequest req = ParseArgs(args, true);
                if (BatchMode)
                    RunBatchMode();
                else
                    ProcessSingleEvent(req);
            }
            catch
            {
                EnvironmentUtils.Invalid();
            }
        }


        static LogAppendRequest ParseArgs(string[] args, bool cmd = false)
        {
            int argc = args.Length;

            LogAppendRequest req = new LogAppendRequest();
            req.Event = new LogRecord();
            req.LogFilePath = args[argc - 1];

            Dictionary<string, string> argsParsed = new Dictionary<string, string>();
            for (int i = argc - 2; i >= 0; i--)
            {
                if (args[i][0] == '-')
                {
                    string key = args[i].ToUpper();
                    if (argsParsed.ContainsKey(key))
                        continue;   // accept the last value
                    
                    string value = "";
                    if (key != ARG_ARRIVAL && key != ARG_LEAVING)
                    {
                        EnvironmentUtils.Assert(i <= argc - 2);
                        value = args[i + 1];
                    }

                    argsParsed[key] = value;
                }
            }

            if (argsParsed.ContainsKey(ARG_BATCH))
            {
                EnvironmentUtils.Assert(cmd && argsParsed.Count == 1);
                BatchMode = true;
                BatchFilePath = argsParsed[ARG_BATCH];
                return req;
            }
            // -----------------------------

            // insufficiency check
            EnvironmentUtils.Assert(
                argsParsed.ContainsKey(ARG_TIMESTAMP) && argsParsed.ContainsKey(ARG_TOKEN)
                && (argsParsed.ContainsKey(ARG_ARRIVAL) || argsParsed.ContainsKey(ARG_LEAVING))
                && (argsParsed.ContainsKey(ARG_EMPLOYEE) || argsParsed.ContainsKey(ARG_GUEST)));

            // contradiction check
            EnvironmentUtils.Assert(!(argsParsed.ContainsKey(ARG_ARRIVAL) && argsParsed.ContainsKey(ARG_LEAVING)));
            EnvironmentUtils.Assert(!(argsParsed.ContainsKey(ARG_EMPLOYEE) && argsParsed.ContainsKey(ARG_GUEST)));

            req.Token = InputValidator.ValidateAlphanumericString(argsParsed[ARG_TOKEN]);

            req.Event.Timestamp = InputValidator.ValidateAndParseInteger(argsParsed[ARG_TIMESTAMP], 1);
            req.Event.Person = new Person(
                argsParsed.ContainsKey(ARG_EMPLOYEE) ? PersonType.Employee : PersonType.Guest,
                InputValidator.ValidateAlphabeticString(argsParsed.ContainsKey(ARG_EMPLOYEE) ? argsParsed[ARG_EMPLOYEE] : argsParsed[ARG_GUEST])
            );
            req.Event.Type = argsParsed.ContainsKey(ARG_ARRIVAL) ? EventType.Arrival : EventType.Leaving;
            if (argsParsed.ContainsKey(ARG_ROOM))
                req.Event.Room = InputValidator.ValidateAndParseInteger(argsParsed[ARG_ROOM], 0);
            
            return req;
        }


        // used for optimization: process events and update the log in memory & write it to a disk in the end
        static void ProcessBatchEvent(LogAppendRequest req)
        {
            if (!BatchLogs.ContainsKey(req.LogFilePath))
            {
                string log = "";
                if (File.Exists(req.LogFilePath))
                    log = CryptoUtils.AESDecryptFile(req.LogFilePath, req.Token);

                BatchLogs[req.LogFilePath] = new BatchLog();
                BatchLogs[req.LogFilePath].Text = new StringBuilder(log);
                BatchLogs[req.LogFilePath].Token = req.Token;

                BatchLogsLastRecordByPerson[req.LogFilePath] = new Dictionary<PersonType, Dictionary<string, LogRecord>>();
                BatchLogsLastRecordByPerson[req.LogFilePath][PersonType.Employee] = new Dictionary<string, LogRecord>();
                BatchLogsLastRecordByPerson[req.LogFilePath][PersonType.Guest] = new Dictionary<string, LogRecord>();

                LogRecord lastRecord = null, lastRecordPerson = null;
                foreach (LogRecord rec in LogParser.GetAllRecords(log))
                {
                    if (rec.Person.Equals(req.Event.Person))
                        lastRecordPerson = rec;
                    lastRecord = rec;
                }

                BatchLogsLastRecord[req.LogFilePath] = lastRecord;
                BatchLogsLastRecordByPerson[req.LogFilePath][req.Event.Person.Type][req.Event.Person.Name] = lastRecordPerson;
            }
            else
            {
                EnvironmentUtils.Assert(BatchLogs[req.LogFilePath].Token == req.Token);
            }

            if (!BatchLogsLastRecordByPerson[req.LogFilePath][req.Event.Person.Type].ContainsKey(req.Event.Person.Name))
                BatchLogsLastRecordByPerson[req.LogFilePath][req.Event.Person.Type][req.Event.Person.Name] = null;
            
            AssertEvent(req, BatchLogsLastRecord[req.LogFilePath], BatchLogsLastRecordByPerson[req.LogFilePath][req.Event.Person.Type][req.Event.Person.Name]);

            BatchLogs[req.LogFilePath].Text.AppendLine(req.Event.ToString());
            BatchLogsLastRecord[req.LogFilePath] = req.Event;
            BatchLogsLastRecordByPerson[req.LogFilePath][req.Event.Person.Type][req.Event.Person.Name] = req.Event;
        }


        static void ProcessSingleEvent(LogAppendRequest req)
        {
            string log = "";
            if (File.Exists(req.LogFilePath))
                log = CryptoUtils.AESDecryptFile(req.LogFilePath, req.Token);

            LogRecord lastRecord = null, lastRecordPerson = null;
            foreach (LogRecord rec in LogParser.GetAllRecords(log))
            {
                if (rec.Person.Equals(req.Event.Person))
                    lastRecordPerson = rec;
                lastRecord = rec;
            }

            AssertEvent(req, lastRecord, lastRecordPerson);

            // everything is correct, add to log
            StringBuilder sb = new StringBuilder(log);
            sb.AppendLine(req.Event.ToString());
            CryptoUtils.AESEncryptFile(req.LogFilePath, req.Token, sb.ToString());
        }


        static void AssertEvent(LogAppendRequest req, LogRecord lastRecord, LogRecord lastRecordPerson)
        {
            // check correctness
            EnvironmentUtils.Assert(lastRecord == null || lastRecord.Timestamp < req.Event.Timestamp);

            if (lastRecordPerson != null)
            {
                if (lastRecordPerson.Type == EventType.Arrival)
                {
                    if (req.Event.Type == EventType.Arrival)
                    {
                        // No person should enter a room without having left a previous room
                        // No person should enter the gallery as a whole twice
                        EnvironmentUtils.Assert(!lastRecordPerson.IsRoomSpecified() && req.Event.IsRoomSpecified());
                    }
                    else if (req.Event.Type == EventType.Leaving)
                    {
                        if (lastRecordPerson.IsRoomSpecified())
                        {
                            // No person should leave the gallery/any other room without first leaving the last room they entered
                            EnvironmentUtils.Assert(req.Event.IsRoomSpecified() && req.Event.Room == lastRecordPerson.Room);
                        }
                        else
                        {
                            // Person should leave gallery as a whole
                            EnvironmentUtils.Assert(!req.Event.IsRoomSpecified());
                        }
                    }
                }
                else if (lastRecordPerson.Type == EventType.Leaving)
                {
                    if (req.Event.Type == EventType.Arrival)
                    {
                        if (lastRecordPerson.IsRoomSpecified())
                        {
                            // Person should enter some room
                            EnvironmentUtils.Assert(req.Event.IsRoomSpecified());
                        }
                        else
                        {
                            // No person should enter a room without first entering the gallery
                            EnvironmentUtils.Assert(!req.Event.IsRoomSpecified());
                        }
                    }
                    else if (req.Event.Type == EventType.Leaving)
                    {
                        // No person should leave the gallery/any other room without first leaving the last room they entered
                        // No person should leave the gallery as a whole twice
                        EnvironmentUtils.Assert(lastRecordPerson.IsRoomSpecified() && !req.Event.IsRoomSpecified());
                    }
                }
            }
            else
            {
                // No person should enter a room without first entering the gallery.
                EnvironmentUtils.Assert(req.Event.Type == EventType.Arrival && !req.Event.IsRoomSpecified());
            }
        }


        static void RunBatchMode()
        {
            BatchLogs = new Dictionary<string, BatchLog>();
            BatchLogsLastRecord = new Dictionary<string, LogRecord>();
            BatchLogsLastRecordByPerson = new Dictionary<string, Dictionary<PersonType, Dictionary<string, LogRecord>>>();

            string line;
            using (StreamReader sr = new StreamReader(BatchFilePath))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    try
                    {
                        string[] args = line.Split(' ');
                        LogAppendRequest req = ParseArgs(args);
                        ProcessBatchEvent(req);
                    }
                    catch
                    {
                        EnvironmentUtils.Invalid(false);
                    }
                }
            }

            foreach (var log in BatchLogs)
                CryptoUtils.AESEncryptFile(log.Key, log.Value.Token, log.Value.Text.ToString());
        }
	}
}