﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using common;

namespace logread
{
    public class LogReadQueryHandler
    {
        public static void ProcessQuery(LogQuery query, string log)
        {
            if (query.Type == QueryType.State)
                ProcessStateQuery(query, log);
            else if (query.Type == QueryType.PersonRooms)
                ProcessRoomsQuery(query, log);
            else if (query.Type == QueryType.PersonTime)
                ProcessTimeQuery(query, log);
            else if (query.Type == QueryType.PersonsIntersection)
                ProcessIntersectionQuery(query, log);
        }


        static void ProcessStateQuery(LogQuery query, string log)
        {
            var persons = new Dictionary<PersonType, HashSet<string>>();
            persons[PersonType.Employee] = new HashSet<string>();
            persons[PersonType.Guest] = new HashSet<string>();

            var rooms = new SortedDictionary<int, HashSet<string>>();

            foreach (var rec in LogParser.GetAllRecords(log))
            {
                if (!rec.IsRoomSpecified())
                {
                    if (rec.Type == EventType.Arrival)
                        persons[rec.Person.Type].Add(rec.Person.Name);
                    else if (rec.Type == EventType.Leaving)
                        persons[rec.Person.Type].Remove(rec.Person.Name);
                }
                else
                {
                    if (!rooms.ContainsKey(rec.Room))
                        rooms[rec.Room] = new HashSet<string>();
                    
                    if (rec.Type == EventType.Arrival)
                        rooms[rec.Room].Add(rec.Person.Name);
                    else if (rec.Type == EventType.Leaving)
                        rooms[rec.Room].Remove(rec.Person.Name);
                }
            }

            Console.WriteLine(CollectionToSortedString(persons[PersonType.Employee]));
            Console.WriteLine(CollectionToSortedString(persons[PersonType.Guest]));

            foreach (var room in rooms)
            {
                if (room.Value.Count == 0)
                    continue;
                
                StringBuilder sb = new StringBuilder();
                sb.Append(room.Key).Append(": ");
                sb.Append(CollectionToSortedString(room.Value));
                Console.WriteLine(sb.ToString());
            }
        }


        static void ProcessRoomsQuery(LogQuery query, string log)
        {
            var rooms = new List<int>();
            foreach (var rec in LogParser.GetAllRecords(log))
                if (rec.Person.Equals(query.Persons[0]) && rec.Type == EventType.Arrival && rec.IsRoomSpecified())
                    rooms.Add(rec.Room);
            
            Console.WriteLine(string.Join(",", rooms));
        }


        static void ProcessTimeQuery(LogQuery query, string log)
        {
            bool exists = false;
            int totalTime = 0;
            int lastArrivalTimestamp = 0;
            int lastTimestamp = 0;
            foreach (var rec in LogParser.GetAllRecords(log))
            {
                lastTimestamp = rec.Timestamp;

                if (rec.Person.Equals(query.Persons[0]))
                {
                    if (rec.Type == EventType.Arrival && !rec.IsRoomSpecified())
                    {
                        exists = true;
                        lastArrivalTimestamp = lastTimestamp;
                    }
                    else if (rec.Type == EventType.Leaving && !rec.IsRoomSpecified())
                    {
                        totalTime += lastTimestamp - lastArrivalTimestamp;
                        lastArrivalTimestamp = 0;
                    }
                }
            }

            if (exists)
            {
                if (lastArrivalTimestamp > 0)
                    totalTime += lastTimestamp - lastArrivalTimestamp;
                
                Console.WriteLine(totalTime);
            }
        }


        static void ProcessIntersectionQuery(LogQuery query, string log)
        {
            var persons = new Dictionary<PersonType, HashSet<string>>();
            persons[PersonType.Employee] = new HashSet<string>();
            persons[PersonType.Guest] = new HashSet<string>();

            foreach (var person in query.Persons)
                persons[person.Type].Add(person.Name);

            int personsNeededCount = persons[PersonType.Employee].Count + persons[PersonType.Guest].Count;

            var roomsHistory = new Dictionary<int, PersonsCount>();
            var roomsIntersect = new HashSet<int>();

            foreach (var rec in LogParser.GetAllRecords(log))
            {
                if (!rec.IsRoomSpecified())
                    continue;

                if (!roomsHistory.ContainsKey(rec.Room))
                    roomsHistory[rec.Room] = new PersonsCount();
                
                if (persons[rec.Person.Type].Contains(rec.Person.Name))
                {
                    if (rec.Type == EventType.Arrival)
                        roomsHistory[rec.Room].PersonsNeeded++;
                    else if (rec.Type == EventType.Leaving)
                        roomsHistory[rec.Room].PersonsNeeded--;
                }
                else
                {
                    if (rec.Type == EventType.Arrival)
                        roomsHistory[rec.Room].PersonsOthers++;
                    else if (rec.Type == EventType.Leaving)
                        roomsHistory[rec.Room].PersonsOthers--;
                }

                if (roomsHistory[rec.Room].PersonsNeeded == personsNeededCount && roomsHistory[rec.Room].PersonsOthers == 0)
                    roomsIntersect.Add(rec.Room);
            }

            Console.WriteLine(CollectionToSortedString(roomsIntersect));
        }


        static string CollectionToSortedString<T>(IEnumerable<T> coll)
        {
            var arr = coll.ToArray();
            Array.Sort(arr);
            return string.Join(",", arr);
        }


        class PersonsCount
        {
            public int PersonsNeeded;
            public int PersonsOthers;
        }
    }
}

