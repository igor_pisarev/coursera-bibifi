﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using common;

namespace logread
{
    class LogReadRequest
    {
        public string Token;
        public string LogFilePath;
        public LogQuery Query;
    }

    class LogRead
    {
        const string ARG_TOKEN = "-K";
        const string ARG_EMPLOYEE = "-E";
        const string ARG_GUEST = "-G";
        const string ARG_STATE = "-S";
        const string ARG_ROOMS = "-R";
        const string ARG_TIME = "-T";
        const string ARG_INTERSECT = "-I";


        public static void Main(string[] args)
        {
            if (args.Length < 4)
                EnvironmentUtils.Invalid();

            try
            {
                LogReadRequest req = ParseArgs(args);
                ProcessQuery(req);
            }
            catch
            {
                EnvironmentUtils.Invalid();
            }
        }


        static LogReadRequest ParseArgs(string[] args)
        {
            int argc = args.Length;

            LogReadRequest req = new LogReadRequest();
            req.Query = new LogQuery();
            req.LogFilePath = args[argc - 1];

            Dictionary<string, string> argsParsed = new Dictionary<string, string>();
            List<Person> persons = new List<Person>();
            for (int i = 0; i < argc; i++)
            {
                if (args[i][0] == '-')
                {
                    string key = args[i];
                    string value = "";
                    if (key == ARG_TOKEN || key == ARG_EMPLOYEE || key == ARG_GUEST)
                    {
                        EnvironmentUtils.Assert(i <= argc - 2);
                        value = args[i + 1];
                    }
                    argsParsed[key] = value;

                    if (key == ARG_EMPLOYEE || key == ARG_GUEST)
                        persons.Add(new Person(key == ARG_EMPLOYEE ? PersonType.Employee : PersonType.Guest, value));
                }
            }

            // contradiction & insufficiency check
            EnvironmentUtils.Assert(argsParsed.ContainsKey(ARG_TOKEN));
            
            int cnt = 0;
            cnt += argsParsed.ContainsKey(ARG_STATE) ? 1 : 0;
            cnt += argsParsed.ContainsKey(ARG_ROOMS) ? 1 : 0;
            cnt += argsParsed.ContainsKey(ARG_TIME) ? 1 : 0;
            cnt += argsParsed.ContainsKey(ARG_INTERSECT) ? 1 : 0;
            EnvironmentUtils.Assert(cnt == 1);

            if (!argsParsed.ContainsKey(ARG_STATE))
            {
                EnvironmentUtils.Assert(argsParsed.ContainsKey(ARG_EMPLOYEE) || argsParsed.ContainsKey(ARG_GUEST));

                if (!argsParsed.ContainsKey(ARG_INTERSECT))
                    EnvironmentUtils.Assert(!(argsParsed.ContainsKey(ARG_EMPLOYEE) && argsParsed.ContainsKey(ARG_GUEST)));
            }
            
            // ---------------------------

            req.Token = InputValidator.ValidateAlphanumericString(argsParsed[ARG_TOKEN]);

            req.Query.Persons = new List<Person>();
            if (argsParsed.ContainsKey(ARG_STATE))
            {
                req.Query.Type = QueryType.State;
            }
            else if (argsParsed.ContainsKey(ARG_ROOMS))
            {
                req.Query.Type = QueryType.PersonRooms;
                InputValidator.ValidateAlphabeticString(persons[persons.Count - 1].Name);
                req.Query.Persons.Add(persons[persons.Count - 1]);
            }
            else if (argsParsed.ContainsKey(ARG_TIME))
            {
                req.Query.Type = QueryType.PersonTime;
                InputValidator.ValidateAlphabeticString(persons[persons.Count - 1].Name);
                req.Query.Persons.Add(persons[persons.Count - 1]);
            }
            else if (argsParsed.ContainsKey(ARG_INTERSECT))
            {
                req.Query.Type = QueryType.PersonsIntersection;
                foreach (Person person in persons)
                    InputValidator.ValidateAlphabeticString(person.Name);
                req.Query.Persons = persons;
            }

            return req;
        }


        static void ProcessQuery(LogReadRequest req)
        {
            string log = "";
            if (!File.Exists(req.LogFilePath))
                EnvironmentUtils.IntegrityViolation();

            try
            {
                log = CryptoUtils.AESDecryptFile(req.LogFilePath, req.Token);
            }
            catch
            {
                EnvironmentUtils.IntegrityViolation();
            }

            LogReadQueryHandler.ProcessQuery(req.Query, log);
        }
    }
}