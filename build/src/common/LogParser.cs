﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace common
{
    public enum PersonType
    {
        None,
        Employee,
        Guest
    }
    public enum EventType
    {
        None,
        Arrival,
        Leaving
    }

    public class Person
    {
        public readonly PersonType Type = PersonType.None;
        public readonly string Name;

        public Person() {}

        public Person(PersonType type, string name)
        {
            Type = type;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            Person p = (Person)obj;
            return (Type == p.Type && Name == p.Name);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class LogRecord
    {
        public int Timestamp;
        public Person Person;
        public EventType Type = EventType.None;
        public int Room = -1;

        public LogRecord() {}

        public LogRecord(string s)
        {
            string[] t = s.Split(',');
            int cnt = 0;
            Timestamp = Convert.ToInt32(t[cnt++]);
            Person = new Person((PersonType)Convert.ToInt32(t[cnt++]), t[cnt++]);
            Type = (EventType)Convert.ToInt32(t[cnt++]);
            if (cnt < t.Length)
                Room = Convert.ToInt32(t[cnt++]);
        }

        public bool IsRoomSpecified()
        {
            return Room >= 0;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Timestamp).Append(',');
            sb.Append((int)Person.Type).Append(',').Append(Person.Name).Append(',');
            sb.Append((int)Type);
            if (Room >= 0)
                sb.Append(',').Append(Room.ToString());
            return sb.ToString();
        }
    }

    public enum QueryType
    {
        None,
        State,
        PersonRooms,
        PersonTime,
        PersonsIntersection
    }
    public class LogQuery
    {
        public QueryType Type = QueryType.None;
        public List<Person> Persons;
    }

    public class LogParser
    {
        public static IEnumerable<LogRecord> GetAllRecords(string log)
        {
            string line;
            using (StringReader sr = new StringReader(log))
                while ((line = sr.ReadLine()) != null && line != "")
                    yield return new LogRecord(line);
        }
    }
}
