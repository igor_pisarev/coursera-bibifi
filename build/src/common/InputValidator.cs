﻿using System;
using System.Linq;

namespace common
{
    public class InputValidator
    {
        public static string ValidateAlphabeticString(string s)
        {
            EnvironmentUtils.Assert(s.All(char.IsLetter));
            return s;
        }


        public static string ValidateAlphanumericString(string s)
        {
            EnvironmentUtils.Assert(s.All(char.IsLetterOrDigit));
            return s;
        }


        public static int ValidateAndParseInteger(string s, int min, int max = 1073741823)
        {
            int x = Convert.ToInt32(s);
            EnvironmentUtils.Assert(x >= min && x <= max);
            return x;
        }
    }
}

