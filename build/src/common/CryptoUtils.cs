﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Collections;
using System.Security.Cryptography;

namespace common
{
    public class CryptoUtils
    {
        static byte[] GetHash(string text)
        {
            using (MD5 md5Hash = MD5.Create())
                return md5Hash.ComputeHash(Encoding.UTF8.GetBytes(text));
        }


        static bool ValidateHashes(byte[] h1, byte[] h2)
        {
            return StructuralComparisons.StructuralEqualityComparer.Equals(h1, h2);
        }


        public static void AESEncryptFile(string path, string token, string text)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (AesManaged aes = new AesManaged())
                {
                    aes.Key = GetHash(token);
                    aes.GenerateIV();

                    fs.Write(aes.IV, 0, aes.IV.Length);

                    ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);
                    using (CryptoStream cs = new CryptoStream(fs, enc, CryptoStreamMode.Write))
                    {
                        string compressed = StringCompressor.CompressString(text);
                        byte[] mac = GetHash(compressed);
                        cs.Write(mac, 0, mac.Length);

                        using (StreamWriter sw = new StreamWriter(cs))
                            sw.Write(compressed);
                    }
                }
            }
        }


        public static string AESDecryptFile(string path, string token)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (AesManaged aes = new AesManaged())
                {
                    aes.Key = GetHash(token);
                    byte[] IV = new byte[aes.BlockSize / 8];
                    fs.Read(IV, 0, IV.Length);
                    aes.IV = IV;

                    ICryptoTransform dec = aes.CreateDecryptor(aes.Key, aes.IV);
                    using (CryptoStream cs = new CryptoStream(fs, dec, CryptoStreamMode.Read))
                    {
                        byte[] mac = new byte[16];
                        cs.Read(mac, 0, mac.Length);

                        using (StreamReader sr = new StreamReader(cs))
                        {
                            string text = sr.ReadToEnd();
                            EnvironmentUtils.Assert(ValidateHashes(GetHash(text), mac), "invalid MAC");
                            return StringCompressor.DecompressString(text);
                        }
                    }
                }
            }
        }

        internal static class StringCompressor
        {
            public static string CompressString(string text)
            {
                byte[] buffer = Encoding.UTF8.GetBytes(text);
                var memoryStream = new MemoryStream();
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                    gZipStream.Write(buffer, 0, buffer.Length);

                memoryStream.Position = 0;

                var compressedData = new byte[memoryStream.Length];
                memoryStream.Read(compressedData, 0, compressedData.Length);

                var gZipBuffer = new byte[compressedData.Length + 4];
                Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
                Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
                return Convert.ToBase64String(gZipBuffer);
            }


            public static string DecompressString(string compressedText)
            {
                byte[] gZipBuffer = Convert.FromBase64String(compressedText);
                using (var memoryStream = new MemoryStream())
                {
                    int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                    memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                    var buffer = new byte[dataLength];

                    memoryStream.Position = 0;
                    using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                        gZipStream.Read(buffer, 0, buffer.Length);

                    return Encoding.UTF8.GetString(buffer);
                }
            }
        }
    }
}