﻿using System;

namespace common
{
    public class EnvironmentUtils
    {
        static void PrintError(string msg, bool needExit)
        {
            Console.WriteLine(msg);
            if (needExit)
                Environment.Exit(255);
        }

        public static void Invalid(bool needExit = true)
        {
            PrintError("invalid", needExit);
        }

        public static void IntegrityViolation(bool needExit = true)
        {
            PrintError("integrity violation", needExit);
        }

        public static void Assert(bool b, string errorMsg = "")
        {
            if (!b)
                throw new Exception(errorMsg);
        }
    }
}

